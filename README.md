**SSM Project**


> aws-ssm-agent playbook will install SSM agent on RHEL, Debian and windows flavours


**ssm.yaml**
```
---
- name: SSM agent uptake
  hosts: ssm
  become: yes
  become_method: sudo
  roles:
    - aws-ssm-agent 

```

SSM project directory structure:

```
ssm-ansible
--> roles
    --> aws-ssm-agent
        ---> tasks
        ---> handlers
        ---> defaults
        ---> files
        ---> templates
        ---> vars
```

**Steps needs to be perform:**

```
1. All all the servers to that inventory group 
2. Run the playbook
```

`ansible-playbook -i inv ssm.yaml`
